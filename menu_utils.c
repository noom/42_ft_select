/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 14:40:26 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/08 10:54:49 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	update_selected(t_menu *menu, int i)
{
	t_silist	*iter;

	iter = menu->selected;
	while (iter)
	{
		if (iter->data > i)
			iter->data--;
		iter = iter->next;
	}
	update_pos(menu);
}

int			get_menu_len(char **map)
{
	int		line_len;
	int		total_len;
	int		map_len;
	int		max_len;
	int		i;

	total_len = 0;
	map_len = 0;
	max_len = 0;
	i = -1;
	while (map[++i])
	{
		line_len = ft_strlen(map[i]);
		if (line_len > max_len)
			max_len = line_len;
		map_len++;
	}
	total_len = (max_len + 1) * map_len;
	return (total_len);
}

int			is_selected(t_silist *list, int i)
{
	t_silist	*iter;

	iter = list;
	while (iter)
	{
		if (iter->data == i)
			return (1);
		iter = iter->next;
	}
	return (0);
}

void		remove_select_item(t_silist **list, int i)
{
	t_silist	*iter;
	t_silist	*before;

	iter = *list;
	before = NULL;
	while (iter)
	{
		if (iter->data == i)
		{
			if (before == NULL)
				*list = iter->next;
			else
				before->next = iter->next;
			free(iter);
		}
		before = iter;
		iter = iter->next;
	}
}

void		remove_map_item(t_menu *menu)
{
	int		i;
	size_t	len;
	char	**new;
	int		ni;
	int		mi;

	i = get_i(menu);
	remove_select_item(&menu->selected, i);
	update_selected(menu, i);
	len = ft_ntsarr_len(menu->map) - 1;
	if ((new = (char**)malloc(sizeof(char*) * (len + 1))) == NULL)
		return ;
	new[len] = NULL;
	mi = -1;
	ni = 0;
	while (menu->map[++mi])
	{
		if (i != mi)
			new[ni++] = menu->map[mi];
		else
			free(menu->map[mi]);
	}
	free(menu->map);
	menu->map = new;
	menu->map_len--;
}
