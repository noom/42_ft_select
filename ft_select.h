/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 13:10:52 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/08 10:41:46 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include "libft.h"

# include <unistd.h>
# include <stdio.h>
# include <termcap.h>
# include <termios.h>
# include <sys/ioctl.h>
# include <fcntl.h>

# define UNUSED(x) (void)(x)

# define S_READ_SIZE	3
# define S_K_UP			"\x1B[A"
# define S_K_DOW		"\x1B[B"
# define S_K_LEF		"\x1B[D"
# define S_K_RIG		"\x1B[C"
# define S_K_ESC		'\e'
# define S_K_DEL		127
# define S_K_BAC		126
# define S_K_RET		10
# define S_K_SPA		' '

typedef struct	s_caps
{
	char	*clr;
	char	*gto;
	char	*sta;
	char	*ste;
	char	*hic;
	char	*shc;
	char	*sav;
	char	*res;
	char	*ula;
	char	*ule;
}				t_caps;

typedef struct	s_point
{
	int	x;
	int	y;
}				t_point;

typedef struct	s_menu
{
	char		**map;
	int			map_len;
	t_point		p;
	int			pad;
	int			width;
	int			height;
	t_point		max;
	t_silist	*selected;
	int			fd;
}				t_menu;

/*
** manage_term.c
*/
void			configure_term(void);
void			restore_term(void);
void			exit_select(int show_map);
int				get_winsize(int *width, int *height);

/*
** sighandlers.c
*/
void			register_signals(void);

/*
** menu.c
*/
void			print_map(t_menu *menu);
void			menu_init(t_menu *menu, int argc, char **argv, int fd);
int				menu_event(t_menu *menu, char c[3]);
void			menu_draw(t_menu *menu, t_caps *caps);

/*
** menu_utils.c
*/
int				get_menu_len(char **map);
int				is_selected(t_silist *list, int i);
void			remove_select_item(t_silist **list, int i);
void			remove_map_item(t_menu *menu);

/*
** menu_move.c
*/
void			move_up(t_menu *menu);
void			move_down(t_menu *menu);
void			move_left(t_menu *menu);
void			move_right(t_menu *menu);

/*
** calcs.c
*/
int				get_padding(char **map);
int				get_i(t_menu *menu);
t_point			get_xy(t_menu *menu, int i);
void			update_pos(t_menu *menu);
int				cmp(int d, int d2);

/*
** utils.c
*/
struct termios	*save_orig_termios(struct termios *orig_termios);
t_menu			*save_menu(t_menu *menu);
t_caps			*save_caps(t_caps *caps);
char			**make_map(int argc, char **argv);
void			put_padding(int current, int padding, int fd);

#endif
