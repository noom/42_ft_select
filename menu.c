/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 21:29:36 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/08 10:55:05 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	toggle_pos(t_menu *menu)
{
	int		i;

	i = get_i(menu);
	if (is_selected(menu->selected, i))
		remove_select_item(&menu->selected, i);
	else
	{
		ft_silistinsn_s(&menu->selected, i, &cmp);
	}
	move_right(menu);
}

void		print_map(t_menu *menu)
{
	t_silist	*iter;
	int			first;

	iter = menu->selected;
	first = 1;
	while (iter)
	{
		if (first)
		{
			first = 0;
			ft_putstr_fd(menu->map[iter->data], STDOUT_FILENO);
		}
		else
			ft_put_fd(STDOUT_FILENO, " %s", menu->map[iter->data]);
		iter = iter->next;
	}
}

void		menu_init(t_menu *menu, int argc, char **argv, int fd)
{
	menu->map = make_map(argc, argv);
	menu->map_len = argc - 1;
	menu->p.x = 1;
	menu->p.y = 1;
	menu->pad = get_padding(menu->map) + 2;
	get_winsize(&menu->width, &menu->height);
	menu->max.x = (menu->width / menu->pad);
	menu->max.x = ((menu->max.x == 0) ? 1 : menu->max.x);
	menu->max.y = (menu->map_len / menu->max.x);
	menu->max.y += (menu->map_len % menu->max.x > 0 ? 1 : 0);
	menu->selected = NULL;
	menu->fd = fd;
}

int			menu_event(t_menu *menu, char *c)
{
	if (c[0] == S_K_ESC && c[1] == '[')
	{
		if (c[2] == 'A')
			move_up(menu);
		else if (c[2] == 'B')
			move_down(menu);
		else if (c[2] == 'D')
			move_left(menu);
		else if (c[2] == 'C')
			move_right(menu);
	}
	else if (c[0] == S_K_SPA)
		toggle_pos(menu);
	else if (c[0] == S_K_RET)
		return (2);
	else if (c[0] == S_K_ESC)
		return (0);
	else if (c[0] == S_K_BAC || c[0] == S_K_DEL)
	{
		remove_map_item(menu);
		if (menu->map_len == 0)
			exit_select(0);
	}
	return (1);
}

void		menu_draw(t_menu *menu, t_caps *caps)
{
	int		select_i;
	int		map_i;
	int		cur_len;

	ft_putstr_fd(caps->clr, menu->fd);
	select_i = get_i(menu);
	map_i = -1;
	cur_len = 0;
	while (menu->map[++map_i])
	{
		cur_len += menu->pad;
		if (cur_len > menu->width)
		{
			cur_len = menu->pad;
			ft_putstr_fd("\r\n", menu->fd);
		}
		if (is_selected(menu->selected, map_i))
			ft_putstr_fd(caps->sta, menu->fd);
		if (get_i(menu) == map_i)
			ft_put_fd(menu->fd, "\e[38;2;200;90;240m%s", caps->ula);
		ft_put_fd(menu->fd, "%s\e[0m", menu->map[map_i]);
		put_padding(ft_strlen(menu->map[map_i]), menu->pad, menu->fd);
	}
}
