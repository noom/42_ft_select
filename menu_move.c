/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu_move.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 12:02:31 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/07 12:25:21 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void		move_up(t_menu *menu)
{
	menu->p.y--;
	if (menu->p.y < 1)
		menu->p.y = menu->max.y;
	if (get_i(menu) >= menu->map_len)
		menu->p.y = menu->max.y - 1;
}

void		move_down(t_menu *menu)
{
	menu->p.y++;
	if (menu->p.y > menu->max.y)
		menu->p.y = 1;
	if (get_i(menu) >= menu->map_len)
		menu->p.y = 1;
}

void		move_left(t_menu *menu)
{
	menu->p.x--;
	if (menu->p.x < 1 && menu->p.y == 1)
		menu->p = get_xy(menu, menu->map_len - 1);
	else if (menu->p.x < 1)
	{
		menu->p.x = menu->max.x;
		menu->p.y--;
	}
}

void		move_right(t_menu *menu)
{
	menu->p.x++;
	if (get_i(menu) >= menu->map_len)
		menu->p = get_xy(menu, 0);
	if (menu->p.x > menu->max.x)
	{
		menu->p.x = 1;
		menu->p.y++;
	}
}
