/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sighandlers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 15:57:07 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/08 10:44:00 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	sigint_handler(int sig)
{
	UNUSED(sig);
	exit_select(0);
}

static void	sigwinch_handler(int sig)
{
	t_menu	*menu;
	t_caps	*caps;
	int		old_i;

	UNUSED(sig);
	menu = save_menu(NULL);
	caps = save_caps(NULL);
	old_i = get_i(menu);
	get_winsize(&menu->width, &menu->height);
	menu->max.x = (menu->width / menu->pad);
	menu->max.x = ((menu->max.x == 0) ? 1 : menu->max.x);
	menu->max.y = (menu->map_len / menu->max.x);
	menu->max.y += (menu->map_len % menu->max.x > 0 ? 1 : 0);
	menu->p = get_xy(menu, old_i);
	menu_draw(menu, caps);
}

static void	sigstp_handler(int sig)
{
	struct termios	*orig_termios;
	char			cp[2];

	UNUSED(sig);
	orig_termios = save_orig_termios(NULL);
	cp[0] = orig_termios->c_cc[VSUSP];
	cp[1] = '\0';
	restore_term();
	signal(SIGTSTP, SIG_DFL);
	ioctl(STDIN_FILENO, TIOCSTI, cp);
}

static void	sigcont_handler(int sig)
{
	t_menu	*menu;
	t_caps	*caps;

	UNUSED(sig);
	menu = save_menu(NULL);
	caps = save_caps(NULL);
	configure_term();
	signal(SIGTSTP, &sigstp_handler);
	menu_draw(menu, caps);
}

void		register_signals(void)
{
	int	i;

	i = -1;
	while (++i < 32)
	{
		if (i != SIGWINCH && i != SIGTSTP && i != SIGCONT)
			signal(i, &sigint_handler);
	}
	signal(SIGINT, &sigint_handler);
	signal(SIGWINCH, &sigwinch_handler);
	signal(SIGTSTP, &sigstp_handler);
	signal(SIGCONT, &sigcont_handler);
}
