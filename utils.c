/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 14:51:38 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/07 13:46:39 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

struct termios	*save_orig_termios(struct termios *orig_termios)
{
	static struct termios	*saved_orig_termios = NULL;

	if (orig_termios != NULL)
		saved_orig_termios = orig_termios;
	return (saved_orig_termios);
}

t_menu			*save_menu(t_menu *menu)
{
	static t_menu	*saved_menu = NULL;

	if (menu != NULL)
		saved_menu = menu;
	return (saved_menu);
}

t_caps			*save_caps(t_caps *caps)
{
	static t_caps	*saved_caps = NULL;

	if (caps != NULL)
		saved_caps = caps;
	return (saved_caps);
}

char			**make_map(int argc, char **argv)
{
	char	**map;
	int		i;

	if ((map = (char**)malloc(sizeof(char*) * (argc))) == NULL)
		return (NULL);
	map[argc - 1] = NULL;
	i = 0;
	while (*(++argv))
		map[i++] = ft_strdup(*argv);
	return (map);
}

void			put_padding(int current, int padding, int fd)
{
	while (current++ < padding)
		ft_putchar_fd(' ', fd);
}
