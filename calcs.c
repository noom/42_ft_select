/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calcs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 19:05:49 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/07 16:33:56 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int		get_padding(char **map)
{
	int		line_len;
	int		max_len;
	int		i;

	max_len = 0;
	i = -1;
	while (map[++i])
	{
		line_len = ft_strlen(map[i]);
		if (line_len > max_len)
			max_len = line_len;
	}
	return (max_len);
}

int		get_i(t_menu *menu)
{
	return (((menu->width / menu->pad) * (menu->p.y - 1)) + (menu->p.x - 1));
}

t_point	get_xy(t_menu *menu, int i)
{
	t_point	p;

	p.x = (i % menu->max.x) + 1;
	p.y = (i / menu->max.x) + 1;
	return (p);
}

void	update_pos(t_menu *menu)
{
	if (get_i(menu) > menu->map_len - 2)
		menu->p = get_xy(menu, menu->map_len - 2);
}

int		cmp(int d, int d2)
{
	return (d < d2);
}
