/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_term.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 22:23:21 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/07 17:15:51 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	exit_select(int show_map)
{
	t_menu	*menu;

	menu = save_menu(NULL);
	restore_term();
	close(menu->fd);
	if (show_map)
		print_map(menu);
	ft_silistdel(&menu->selected);
	ft_ntsarr_free(&menu->map);
	exit(0);
}

void	configure_term(void)
{
	struct termios	*orig_termios;
	t_caps			*caps;
	t_menu			*menu;
	struct termios	raw;

	orig_termios = save_orig_termios(NULL);
	caps = save_caps(NULL);
	menu = save_menu(NULL);
	raw = *orig_termios;
	raw.c_lflag &= ~(ECHO | ICANON);
	raw.c_oflag &= ~(OPOST);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
	ft_putstr_fd(caps->hic, menu->fd);
	ft_putstr_fd(caps->sav, menu->fd);
}

void	restore_term(void)
{
	struct termios	*orig_termios;
	t_caps			*caps;
	t_menu			*menu;

	orig_termios = save_orig_termios(NULL);
	caps = save_caps(NULL);
	menu = save_menu(NULL);
	ft_putstr_fd(caps->res, menu->fd);
	ft_putstr_fd(caps->shc, menu->fd);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, orig_termios);
}

int		get_winsize(int *width, int *height)
{
	struct winsize ws;

	if (ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0)
		return (1);
	*width = ws.ws_col;
	*height = ws.ws_row;
	return (0);
}
