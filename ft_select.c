/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@protonmail.com>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 13:10:52 by echojnow          #+#    #+#             */
/*   Updated: 2018/03/08 10:37:57 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static int	init_caps(t_caps *caps)
{
	caps->clr = tgetstr("cl", NULL);
	caps->gto = tgetstr("cm", NULL);
	caps->sta = tgetstr("so", NULL);
	caps->ste = tgetstr("se", NULL);
	caps->hic = tgetstr("vi", NULL);
	caps->shc = tgetstr("ve", NULL);
	caps->sav = tgetstr("ti", NULL);
	caps->res = tgetstr("te", NULL);
	caps->ula = tgetstr("us", NULL);
	caps->ule = tgetstr("ue", NULL);
	return (0);
}

static int	init(int argc, char **argv)
{
	t_menu			*menu;
	t_caps			*caps;
	struct termios	*orig_termios;
	char			*term_env;

	menu = save_menu(NULL);
	caps = save_caps(NULL);
	orig_termios = save_orig_termios(NULL);
	term_env = NULL;
	term_env = getenv("TERM");
	if (term_env == NULL)
		return (ft_err(1, "ft_select: $TERM is not defined"));
	else if (tgetent(NULL, getenv("TERM")) != 1)
		return (ft_err(1, "ft_select: could not load $TERM entry"));
	tcgetattr(menu->fd, orig_termios);
	register_signals();
	init_caps(caps);
	menu_init(menu, argc, argv, menu->fd);
	return (0);
}

static int	loop(t_caps *caps, t_menu *menu)
{
	int		running;
	char	c[S_READ_SIZE];

	running = 1;
	ft_memset(c, 0, S_READ_SIZE);
	while (running)
	{
		menu_draw(menu, caps);
		read(0, &c, S_READ_SIZE);
		if ((running = menu_event(menu, c)) == 2)
			return (1);
		ft_memset(c, 0, S_READ_SIZE);
	}
	return (0);
}

int			main(int argc, char **argv)
{
	int				fd;
	struct termios	orig_termios;
	t_caps			caps;
	t_menu			menu;

	if (argc < 2)
		return (0);
	fd = STDIN_FILENO;
	if (isatty(STDIN_FILENO))
		fd = open(ttyname(STDIN_FILENO), O_RDWR);
	else
		return (ft_err(1, "ft_select: the terminal is not a tty"));
	save_orig_termios(&orig_termios);
	save_caps(&caps);
	save_menu(&menu);
	if (init(argc, argv) == 1)
		return (1);
	configure_term();
	if (loop(&caps, &menu) == 1)
		exit_select(1);
	else
		exit_select(0);
	return (0);
}
