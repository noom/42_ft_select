# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/30 11:15:13 by echojnow          #+#    #+#              #
#    Updated: 2018/03/07 17:17:58 by echojnow         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select

CC = clang
FLAG = -Wall -Wextra -Werror

INCLUDE = -Ilibft/includes
LIB = -Llibft -lft -ltermcap

SRC = \
	  ft_select.c \
	  manage_term.c \
	  sighandlers.c \
	  menu.c \
	  menu_utils.c \
	  menu_move.c \
	  calcs.c \
	  utils.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(FLAG) $(OBJ) -o $(NAME) $(LIB)

%.o: %.c
	$(CC) $(FLAG) $(INCLUDE) -c $^

clean:
	$(RM) $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

run: all
	@echo -------------------
	@./$(NAME) .

vim:
	nvim Makefile ft_select.h $(SRC)
